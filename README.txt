You will have to set up your computer to work with everything. This is some of the stuff I use currently at work, so you will be learning a few things that people in college do not even know how to use. It might take a few tries to be able to learn everything, but it is worth it. 

1. Install GIT. 

	1a. Copy and Paste 'https://github.com/git-for-windows/git/releases/download/v2.5.0.windows.1/Git-2.5.0-64-bit.exe'(without quotes) into address bar.
	1b. Install the downloaded file with all default opens, i.e. just click next the whole time.

2. Install Notepad++ from the following URL: https://notepad-plus-plus.org/repository/6.x/6.8.2/npp.6.8.2.Installer.exe

3. Create an account on 'https://bitbucket.org/' using the username 'alance' (without quotes).

4. Open a folder and go to My Documents and create a folder called 'Repositories' (without quotes). 
	4a. Enter new folder and right click and choose 'GIT Bash here'
	4b. Type into the black terminal windows 'git clone https://sflaherty@bitbucket.org/sflaherty/teachingpython.git'
		4b1. It may ask you for a password. This password should be the one you set for your BitBucket account.
	4c. There should be a new folder called "teachingpython' in your folder. It will have other folders and files in it. 
	4d. Run the following two commands:
		4d1. git config --global user.email "your email in quotes"(include the quotes)
			4d1a. For Example, git config --global user.email "carrottop23@gmail.com" is the line I put for mine.
		4d2. git config --global user.name "Aidan Lance"

5. Open and read 'HowToUseGIT.txt'. This is some basic commands you can use in the black terminal window for GIT. You will have to use some of them in the future.

5. Install Python using the following URL: https://www.python.org/ftp/python/2.7.10/python-2.7.10.amd64.msi

6. Go to the folder 'code' and right click 'HelloWorldExample.py' and choose open in Notepad++. Try to see if you can make sense of the code.
   You can ask me questions if it is totally confusing. 

7. Try to run the 'HelloWorldExample.py' by typing into the 'GIT Bash here' terminal 'python HelloWorldExample.py' (without quotes).
	7a. It should print the words 'Hello World' onto the terminal screen.

7. Use the instructions in 'HowToUseGIT.txt' to upload your new files and 
   message me when you are done. I will be able to download your file and 
   see your results.

If you have any questions you are allowed to ask me at any point. This isn't 
any kind of test or quiz. You should especially ask me for assistance if you
have trouble understanding the installations. This could cause future problems
and it would be better if everything worked correctly. 
